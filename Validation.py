# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 15:49:32 2021

Note:
    This is a buddy file for Unet_Training.py, and it is the validation portion of the model.
    It uses the same data loading structure of Unet_Training, loads the saved model, and predicts
    the segmented image in the file. The figure is plotted and saved alongside the original for now.

@author: kenhu
"""
# This code is for loading the U-Net model, and then applying the model onto a random validation imageset
# The validation imageset has no Mask, so the output is just a segmented image to be evaluated visually

import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from tensorflow import keras
from cv2 import imread, resize, imwrite
import skimage as sk
from skimage import io, color, measure

model = keras.models.load_model('Models/Cross_Section_Unet4.h5')

SIZE = 128
# img_folder = 'MEAs/Validation/Validation_Images2'


def segmentimages(input_file_name_and_path: str):
    img_filelist = os.listdir(input_file_name_and_path)

    savefolder = input_file_name_and_path + "/Segmented_Images"
    if not os.path.exists(savefolder):
        os.makedirs(savefolder)

    savefolder2 = input_file_name_and_path + "/SidebySide"
    if not os.path.exists(savefolder2):
        os.makedirs(savefolder2)

    cross_table = pd.DataFrame(columns = ['ImageID','Thickness (um)'])

    for i in range(0, len(img_filelist)):
        img = imread(input_file_name_and_path + "/" + img_filelist[i], -1)
        img_orig_size = np.shape(img)[0:2]
        img_resize = resize(img, (SIZE, SIZE))
        img_resize = np.expand_dims(img_resize, axis=0)

        pred_img = model.predict(img_resize)
        pred_img = np.squeeze(pred_img)

        pred_img = resize(pred_img, (img_orig_size[1], img_orig_size[0]))

        imwrite(savefolder + '/SegImage_' + img_filelist[i] + '.tif', pred_img)

        # Plot Figure
        plt.figure(figsize=[20, 10])

        plt.subplot(1, 3, 1)
        plt.imshow(img)
        plt.title('Original')

        plt.subplot(1, 3, 2)
        plt.imshow(pred_img, cmap='jet')
        plt.title('Predicted Image')
        plt.colorbar(fraction=0.046, pad=0.04)

        plt.subplot(1, 3, 3)
        plt.imshow(img)
        plt.imshow(pred_img, cmap='jet', alpha=0.3)

        plt.title('Overlay Image')

        plt.savefig(savefolder2 + '/SBSImage_' + img_filelist[i] + '.png')
        plt.clf()

        cross = measure_crosssection(pred_img)
        temptable = pd.DataFrame([[img_filelist[i],cross]], columns = ['ImageID','Thickness (um)'])
        cross_table = cross_table.append(temptable)
    cross_table.to_csv(savefolder + '/Fintable.csv')


def measure_crosssection(img):
    pixel_um_ratio = 6.9375

    img_thresh = img > 0.5  # Threshold, to be played with
    img_thresh = img_thresh.astype(
        'uint8') * 255  # Make it into 0,255 uint8 datatype, just familiar for me to work with.

    label_img = sk.measure.label(img_thresh)

    sections = sk.measure.regionprops_table(label_img, img_thresh, properties=['label', 'area', 'coords'])
    sections = pd.DataFrame(sections)

    # Find largest sections
    max_section = sections[sections.area == sections.area.max()]
    max_area = float(max_section['area'])

    # Based on Largest section, find the 'length' based on the lowest and highest row coordinate
    section_coords = max_section.at[0, 'coords']
    L_top = min(section_coords[:, 0])
    L_bot = max(section_coords[:, 0])
    length_total = L_bot - L_top + 1  # length subtracting bottom most coordinate with top most coordinate

    # Calculate Cross-section based on area and dimensions
    area_in_um = max_area / (pixel_um_ratio ** 2)

    length_in_um = length_total / pixel_um_ratio

    cross_in_um = area_in_um / length_in_um
    return cross_in_um

#segmentimages('Images/Validation_Images2/')
