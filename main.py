"""Contains GUI for analyzing cross section images"""

import os
import sys

from PySide2.QtWidgets import (QAction, QApplication, QComboBox, QFileDialog,
                               QGridLayout, QLabel, QMainWindow, QPushButton,
                               QWidget)

from Validation import segmentimages

DEFAULT_ITEM = "..."
LAYER_TYPES = ['Ionomer', 'Catalyst']
SAMPLE_PREP_TYPES = ['Liquid Nitrogen Cracked', 'Grinded and Polished']


class CrossSectionWindow(QMainWindow):
    """Window which contains the CrossSectionUI"""

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Cross-Section Measurement App Select")

        # menu (File, Help)
        self.menu = self.menuBar()
        self.close_gui_only = False
        self.file_menu = self.menu.addMenu("File")
        self.help_menu = self.menu.addMenu("Help")

        # example action; TODO: replace
        do_something_action = QAction("Do Something", self)
        do_something_action.triggered.connect(do_something)
        self.file_menu.addAction(do_something_action)

        self.central_widget = CrossSectionUI()
        self.setCentralWidget(self.central_widget)
        self.show()


def do_something():
    # TODO: remove this; only here to show that you can call a function from the menu
    print("do_something called")


class CrossSectionUI(QWidget):
    """Overall class for gui for selecting measurement file for visualization"""

    def __init__(self):
        super().__init__()
        self.setMinimumSize(300, 100)

        # window layout
        gui_layout = QGridLayout()

        # input file selection dialog
        input_file_button = QPushButton("Measurement File")
        input_file_button.clicked.connect(self.select_input_pressed)
        row = gui_layout.rowCount()
        gui_layout.addWidget(input_file_button, row, 0)
        self.input_file_label = QLabel("None")
        gui_layout.addWidget(self.input_file_label, row, 1)

        # Dropdownbox for Layer Type
        dropdown_layer = QComboBox()
        dropdown_layer.addItems([DEFAULT_ITEM] + LAYER_TYPES)
        gui_layout.addWidget(dropdown_layer, row + 1, 1)
        dropdown_layer_label = QLabel('Layer Type: ')
        gui_layout.addWidget(dropdown_layer_label, row + 1, 0)

        # Dropdownbox for Type of Sample Prep
        dropdown_sp = QComboBox()
        dropdown_sp.addItems([DEFAULT_ITEM] + SAMPLE_PREP_TYPES)
        gui_layout.addWidget(dropdown_sp, row + 2, 1)
        dropdown_sp_label = QLabel('Sample Preperation Method: ')
        gui_layout.addWidget(dropdown_sp_label, row + 2, 0)

        # press this to do the thing
        self.open_button = QPushButton("Filter")
        self.open_button.clicked.connect(self.open_pressed)
        self.open_button.setEnabled(False)
        gui_layout.addWidget(self.open_button, gui_layout.rowCount(), 0, 1, 2)

        # set the window layout
        self.setLayout(gui_layout)

    def select_input_pressed(self):
        """
        This function is called when the 'Measurement File' button is pressed.
        It lets the user select a file to reduce by opening a modal
        file select window.
        """

        # log file select
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)   # Make it select a directory
        dialog.setAcceptMode(QFileDialog.AcceptOpen)
        dialog.setWindowTitle("Select input directory")

        if dialog.exec_():
            file_name = dialog.selectedFiles()[0]
            self.input_file_label.setText(
                os.path.split(file_name)[-1]
            )  # label only shows file name
            self.input_file_label.setToolTip(file_name)  # tool tip shows full path
            self.open_button.setEnabled(True)

    def open_pressed(self):
        """
        This function calls plot_data to create a html file with
        the data from the selected measurement file
        """
        file_name = self.input_file_label.toolTip()
        segmentimages(
            input_file_name_and_path=file_name
        )


if __name__ == "__main__":
    APP = QApplication([])
    WINDOW = CrossSectionWindow()
    sys.exit(APP.exec_())
