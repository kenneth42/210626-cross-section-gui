import pandas as pd
import numpy as np


def maketable(input_file_name_and_path: str):
    df2 = pd.DataFrame(np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]),
                       columns=['a', 'b', 'c'])
    df2.to_csv(input_file_name_and_path + '/new_df.csv')